# ENP-TOK

Official repo of the data presented in the paper: Unlocking Transitional Chinese: Word Segmentation in Modern Historical Texts

## Data

The data is already separated into a training set and a test set. They are available in two formats, Txt for only the raw data already tokenised. And a csv format for more information about the source of the annotated articles, including the publication date.


## Acknowledgment

- This project has received funding from the European Research Council (ERC) under the European Union’s Horizon 2020 research and innovation programme (grant agreement No 788476). 

- The authors wish to acknowledge fruitful discussions with Pierre Magistry. They also thank the annotators, Chang Yu-jun, Chiang Chia-wei, Hsu Wan-lin, and the curator Dr. Sun Huei-min (Institute of Modern History, Academia Sinica)
